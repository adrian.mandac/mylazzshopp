Feature: User Notification

  Scenario Outline: Account notification after creating user account
    Given the client has inputted their <string> and <string2>
    When the system registers the <string> and <string2> inside the database.
    Then the client gets a <string1> that his account has been created.
    Examples:
      | string | string2 | string1 |


