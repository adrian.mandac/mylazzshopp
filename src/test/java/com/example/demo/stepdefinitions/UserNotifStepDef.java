package com.example.demo.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UserNotifStepDef {
    @Given("the client has inputted their <string> and <string{int}>")
    public void theClientHasInputtedTheirStringAndString(int arg0) {
        System.out.println(arg0);
    }

    @When("the system registers the <string> and <string{int}> inside the database.")
    public void theSystemRegistersTheStringAndStringInsideTheDatabase(int arg0) {
        System.out.println(arg0);
    }

    @Then("the client gets a <string{int}> that his account has been created.")
    public void theClientGetsAStringThatHisAccountHasBeenCreated(int arg0) {
        System.out.println(arg0);
    }
}
